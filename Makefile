target: cache.c
	gcc cache.c -lm

test: cache.c
	gcc cache.c -lm
	./a.out -s 128 -l 16 -f addresses.txt -a 4

clean:
	rm ./a.out
