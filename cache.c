#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>

typedef int bool;
#define TRUE 1
#define FALSE 0

int num_addresses;

int logtwo(int x)
{
	double logtwo;
	logtwo = log10(x)/log10(2);
	logtwo = (int) logtwo;
	return(logtwo);
}

int* get_addresses(char * file)
{
	FILE * fp;	
	int i = 0;
	num_addresses = 0;
	int * addresses;
	int address;

	//open file
	fp = fopen(file, "r");
	if(fp == NULL)
	{
		printf("'%s' didn't open!\n", file);
		return NULL;
	}

	//get number of addresses
	while (fscanf(fp, "%x", &address) != EOF)
	{
		num_addresses++;	
	}
	printf("%d addresses in file\n", num_addresses);

	//allocate memory for addresses array
	addresses = malloc(num_addresses * sizeof(int));
	
	//go back to top of file
	rewind(fp);

	//assign addresses from file to elements in addresses array
	while (fscanf(fp, "%x", &addresses[i]) != EOF)
		i++;	

	fclose(fp);
	return addresses;
}

//function to get least recently used line in a set
int LRU_line(int * LRU[], int set, int NUM_LINES)
{
	int min = LRU[set][0];
	int LRU_line = 0;

	int j;
	for (j=0; j<NUM_LINES; j++)
	{
		if (LRU[set][j] < min)
		{
			min = LRU[set][j];
			LRU_line = j;
		}
	}
	return(LRU_line);
}

int main(int argc, char * argv[])
{
	int i;
	int opt;
	int s,l,a;
	char * f;
	int address;
	while ((opt = getopt(argc, argv, "s:l:a:f:")) != -1)
	{
		switch(opt)
		{
			case 's':
				s = atoi(optarg);
				break;
			case 'l':
				l = atoi(optarg);
				break;
			case 'a':
				a = atoi(optarg);
				break;
			case 'f':
				f = optarg;
				break;
		}
	}

	int * addresses = get_addresses(f);
	if( addresses == NULL ) return 1;

	int HITS = 0;
	int MISSES = 0;
	int NUM_SETS = 8/a;
	int NUM_LINES = 8/NUM_SETS;
	
	//create cache and LRU arrays
	int **cache = malloc(NUM_SETS * sizeof(int *));
	int *cache_lines = malloc(NUM_SETS * NUM_LINES * sizeof(int));
	//int cache[NUM_SETS][NUM_LINES];
	int ** LRU = malloc(NUM_SETS * sizeof(int *)); //fill with integers as we go along, to get LRU find the lowest number
	int *LRU_lines = calloc(NUM_SETS * NUM_LINES, sizeof(int));
	for (i=0; i<NUM_SETS; i++)
	{
		cache[i] = &cache_lines[i*NUM_LINES];
		LRU[i] = &LRU_lines[i*NUM_LINES];
		int j;
		for (j=0; j<NUM_LINES; j++)
			cache[i][j] = -1;
	}

	//create masks to get tags and sets
	unsigned int tag, set;
	int tag_mask = ~(~0 << (16-logtwo(NUM_SETS)-logtwo(l))) << (logtwo(NUM_SETS)+logtwo(l));
	int set_mask = ~(~0 << logtwo(NUM_SETS)) << logtwo(l); //~0 is all 1's, shift left by number of set bits to get logtwo(NUM_SETS) 0's, apply ~ to turn them into 1's, then shift left by number of offset bits to get them in the right place to mask with the address 

	//cache simulation
	int count = 0;
	for (i=0; i<num_addresses; i++) 
	{
		count++;

		//get tag and set
		tag = (tag_mask & addresses[i]) >> logtwo(NUM_SETS) + logtwo(l);
		set = (set_mask & addresses[i]) >> logtwo(l);  //apply mask to get set bits then shift right by number of offset bits

		//set = addresses[i] << (16-logtwo(NUM_SETS)-logtwo(l));  //shift left to clear tag bits, 16 since addresses are 16 bytes
		//set = set >> (16-logtwo(l)); //shift back right to clear offset bits
		//printf("%04x goes into set %d\n", addresses[i], set);

		//assume set does not have tag
		bool set_has_tag = FALSE;

		//check to see if set has tag
		int j;
		for (j=0; j<NUM_LINES; j++)
		{
			//if set has tag then we get a cache hit
			if (cache[set][j] == tag)
			{
				set_has_tag = TRUE;
				LRU[set][j] = count;
				HITS++;
				printf("%04x is a hit\n", addresses[i]);
				break;
			}
		}

		//if set doesn't have tag then put tag in least recently used line and incremente number of cache misses
		if (set_has_tag == FALSE)
		{			
			j = LRU_line(LRU, set, NUM_LINES);
			cache[set][j] = tag;
			LRU[set][j] = count;
			MISSES++;
			printf("%04x is a miss\n", addresses[i]);
		}
	}
	
	//print hit rate
	double hit_rate = ((double)HITS)/((double)(HITS+MISSES));
	printf("hit rate = %lf\n", hit_rate);
	printf("MISSES= %d\n", MISSES);
	printf("HITS= %d\n", HITS);

	//free memory
	free(cache_lines);
	free(cache);	
	free(LRU_lines);
	free(LRU);
	free(addresses);

	return 0;
}
