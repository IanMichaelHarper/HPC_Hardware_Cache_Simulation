C code to calculate cache hits and misses.

compile using make.
run as such: ./a.out -s 128 -l 16 -f addresses.txt -a 4 

arguments for -a are 1 for direct mapped, 2 for 2-way, 4 for 4-way and 8 for fully associative.

visualization:
https://www.scss.tcd.ie/Jeremy.Jones/vivio/caches/cache.htm
